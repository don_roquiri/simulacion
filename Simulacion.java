/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author str4ng3r
 */
public class Simulacion {

    public Simulacion() throws HeadlessException {

    }

    public static void main(String[] args) {
        int datos[] = new int[]{
            12, 7, 15, 13, 16, 9, 21, 12, 23, 18,
            10, 8, 9, 24, 10, 2, 14, 20, 11, 7,
            8, 14, 5, 14, 9, 13, 11, 3, 13, 6,
            9, 12, 5, 6, 5, 11, 8, 10, 12, 17
        };

        PruebasEstadisticas pruebas = new PruebasEstadisticas(datos);

        System.out.println("Media : " + pruebas.media);
        System.out.println("Varianza : " + pruebas.varianza);
        System.out.println("Desviacion estandar : " + pruebas.desviacionEstandar);
        pruebas.tablaDeFrecuencias();
    }

}

class PruebasEstadisticas extends JFrame {

    double p = 0d;
    double q = 0d;
    double media = 0d;
    double varianza = 0d;
    double desviacionEstandar;
    int values[];

    public PruebasEstadisticas(int values[]) {
        this.values = values;
        media();
        varianza();
        desviacionEstandar = desviacionEstandar();
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    void media() {
        for (int i = 0; i < values.length; i++) {
            media += values[i];
        }
        media /= values.length;
    }

    void varianza() {
        varianza = 0;
        for (int i = 0; i < values.length; i++) {
            varianza = varianza +  ((Math.pow((values[i] - media), 2))/values.length);
            System.out.println("varianza " + varianza);
        }
    }

    double desviacionEstandar() {
        return Math.sqrt(varianza);
    }

    double geometrica() {
        //p=1/(1+ẍ)
        //q=1-p 
        System.out.println("---------------------------------");
        System.out.println("GEOMETRICA");
        p = 1 / (1 + media);
        q = 1 - p;
        double xi = 0d;
        double random = Math.random();
        xi = Math.log(random) / Math.log(q);
        System.out.println("P = " + p + "\nq = " + q + "\nRandom = " + random + "\nxi = " + xi);
        System.out.println("---------------------------------");
        return xi;
    }

    //Lambda es igual a x testada
    double poisson() {
        //e^(-ẍ)
        System.out.println("---------------------------------");
        System.out.println("POISSON");
        double condicion = Math.exp(-media);
        double producto = 0d;
        int count = 0;
        producto = Math.random();
        do {
            producto *= Math.random();
            count++;
        } while (condicion <= producto);
        System.out.println("El ciclo ah terminado:"
                + "\nEl valor del producto fue: " + producto
                + "\nEl valor de la condicion (e^(-λ)): " + condicion
                + "\nEn la interacción numero: " + (count)
                + "\nXi = " + count);
        System.out.println("---------------------------------");
        return count;
    }

    /**
     * Binomial P = (ẍ-rx²)/ẍ N = ẍ²/(ẍ-rx²)
     *
     */
    double binomial() {
        System.out.println("---------------------------------");
        System.out.println("BINOMIAL");
        p = ((media - varianza) / media);
        System.out.println(p);
        double xi = 0d;
        if (p >= 0) {
            q = Math.pow(media, 2) / (media - varianza);
            for (int i = values.length; i == values.length / 2; i++) {
                xi += values[i] + values[i - 1];
            }
            System.out.println("Xi: " + xi);
        } else {
            xi = 0d;
            System.out.println("Esta prueba no es recomendable para esta muestra de datos");
        }
        System.out.println("---------------------------------");
        return xi;
    }

    double binomialNegativa() {
        System.out.println("---------------------------------");
        System.out.println("Binomial negativa");
        p = (double) (media / varianza);
        System.out.println(p);
        double k = (media * media) / (varianza - media);
        System.out.println("p: " + p + " k: "+k);
        q = 1 - p;
        double xi = 0d;
        double producto = 1d;
        if (k >= 0) {
            for (int i = 0; i < k; i++) {
                producto *= Math.random();
            }
            xi = Math.log(producto) / Math.log(q);
            System.out.println("P = " + p + "\nq = " + q + "\nk  = " + k + "\nproducto = " + producto + "\nxi = " + xi);
            System.out.println("---------------------------------");
        } else {
            xi = 0;
            System.out.println("Esta prueba no es compotente para esta muestra de datos");
        }
        return xi;
    }

    double exponencial() {
        System.out.println("---------------------------------");
        System.out.println("Exponencial");
        double xi = media - Math.log(Math.random());
        System.out.println("xi = " + xi);
        System.out.println("---------------------------------");
        return xi;
    }

    double uniforme() {
        System.out.println("---------------------------------");
        System.out.println("UNIFORME");
        double a = 0d;
        a = media - Math.sqrt(3 * varianza);
        double b = 2 * media - a;
        double xi = a + (b - a) * Math.random();
        System.out.println("a = " + a + "\nb = " + b + "\nxi = " + xi);

        System.out.println("---------------------------------");
        return xi;
    }

    double normal() {
        System.out.println("---------------------------------");
        System.out.println("NORMAL");
        double acumulado = 0d;
        for (int i = 0; i < 12; i++) {
            acumulado += Math.random();
        }
        acumulado -= 6;
        System.out.println("normal " + acumulado);
        double xi = media + (desviacionEstandar * acumulado);
        System.out.println("xi = " + xi);
        System.out.println("---------------------------------");
        return xi;
    }

    double gamma() {
        System.out.println("---------------------------------");
        System.out.println("GAMMA");
        double x = media / varianza;
        double k = media * media / varianza;
        double producto = 1d;
        double xi = 0;
        if (k > 0) {
            for (int i = 0; i < k; i++) {
                producto *= Math.random();
            }
        } else {
            xi = 0;
            System.out.println("Esta prueba no es competente para esta muestra de datos");
        }
        xi = -(1 / x) * Math.log(producto);
        System.out.println("P = " + p + "\nq = " + q + "\nk  = " + k + "\nproducto = " + producto + "\nxi = " + xi);
        System.out.println("---------------------------------");
        return xi;
    }

    double logaritmica() {
        System.out.println("---------------------------------");
        System.out.println("LOGARITMICA");
        double ry = Math.log10(varianza / media * media) + 1;
        double dy = Math.log10(media - (.5d * ry));
        double producto = 1d;
        for (int i = 0; i < 12; i++) {
            producto += Math.random();
        }
        producto -= 6;
        double xi = Math.E * ((dy + ry) * producto);
        System.out.println("\nry  = " + ry + "\ndy = " + dy + "\nxi = " + xi + "\nproducto = " + producto);
        System.out.println("---------------------------------");
        return xi;
    }

    ArrayList<Integer> Datos;
    paqueteDePruebas paquete[];

    public void tablaDeFrecuencias() {
        JTextArea txt = new JTextArea();
        Datos = new ArrayList();
        for (int i = 0; i < values.length; i++) {
            Datos.add(values[i]);
        }

        txt.append("Los datos son:\n" + Datos.toString());
        //System.out.println("El limite inferior es: " + Collections.max(Datos));

        //Calcular el rango
        txt.append("\n--------------------------\n");
        int rango = Collections.max(Datos) - Collections.min(Datos);
        txt.append("Rango: " + rango);

        txt.append("\n--------------------------\n");

        //Calcular numero de intervalos
        int numerodatos = Datos.size();
        int intervalos = (int) Math.round(Math.sqrt(numerodatos));
        txt.append("\nNumero de Intervalos: " + intervalos);

        paquete = new paqueteDePruebas[intervalos - 1];

        txt.append("\n--------------------------\n");

        //Calcular Amplitud de la clase
        int amplitud = (int) (Math.round((double) rango / (double) intervalos));
        txt.append("Amplitud " + amplitud);

        txt.append("\n--------------------------\n");
        //Nuevo rango 

        rango = intervalos * amplitud;
        txt.append("Nuevo rango :" + rango);

        int clase_anterior = 0;
        int frAbs = 0;
        int frAcm = 0;
        int nueva_clase = 0;
        double variables[];
        double errCM[];
        String rangos[] = new String[9];
        for (int i = 0; clase_anterior < (int) Collections.max(Datos); i++) {
            nueva_clase = clase_anterior + (intervalos - 1);

            //Comportamiento observado
            frAbs = frecuenciaAbsoluta(clase_anterior, nueva_clase);
            //Acumulado comportamiento observado
            frAcm += frAbs;

            variables = new double[]{geometrica(), poisson(), binomial(),
                binomialNegativa(), exponencial(), uniforme(),
                normal(), gamma(), logaritmica()};
            errCM = new double[9];
            for (int j = 0; j < variables.length; j++) {
                variables[j] = Math.round(variables[j]);
                double ecm = frAbs - variables[j];
                errCM[j] = Math.pow(ecm, 2);
                System.out.println(errCM[j]);
            }

            paquete[i] = new paqueteDePruebas((clase_anterior) + "," + nueva_clase, frAbs, errCM, variables);

            clase_anterior = nueva_clase;
        }

        add(txt, BorderLayout.SOUTH);
        Tabla tabla = new Tabla(paquete);
        add(tabla.panel, BorderLayout.CENTER);
        pack();
    }

    /**
     * Cuenta los numeros que estan dentro de un rango de datos
     *
     * @param limInf
     * @param limSup
     * @return
     */
    private int frecuenciaAbsoluta(Integer limInf, Integer limSup) {
        int count = 0;
        for (int i = 0; i < Datos.size(); i++) {
            if ((int) Datos.get(i) >= limInf && (int) Datos.get(i) < limSup) {
                count++;
            }
        }
        return count;
    }
}

class paqueteDePruebas {

    String rangos;
    double variablesAleatoriasEstadistica[] = new double[9];
    double errorCuadrado[] = new double[9];
    int frecuencia;

    public paqueteDePruebas(String rangos, int frecuencia, double errorCuadrado[], double variables[]) {
        this.rangos = rangos;
        this.frecuencia = frecuencia;
        this.errorCuadrado = errorCuadrado;
        this.variablesAleatoriasEstadistica = variables;
    }

}

class Tabla {

    JScrollPane panel;
    DefaultTableModel mdl;
    JTable tabla;
    int index;

    public Tabla(paqueteDePruebas arra[]) {

        Object data[][] = new Object[arra.length][20];
        for (int i = 0; i < arra.length; i++) {
            data[i][0] = arra[i].rangos;
            data[i][1] = arra[i].frecuencia;
            data[i][2] = arra[i].variablesAleatoriasEstadistica[0];
            data[i][3] = arra[i].errorCuadrado[0];
            data[i][4] = arra[i].variablesAleatoriasEstadistica[1];
            data[i][5] = arra[i].errorCuadrado[1];
            data[i][6] = arra[i].variablesAleatoriasEstadistica[2];
            data[i][7] = arra[i].errorCuadrado[2];
            data[i][8] = arra[i].variablesAleatoriasEstadistica[3];
            data[i][9] = arra[i].errorCuadrado[3];
            data[i][10] = arra[i].variablesAleatoriasEstadistica[4];
            data[i][11] = arra[i].errorCuadrado[4];
            data[i][12] = arra[i].variablesAleatoriasEstadistica[5];
            data[i][13] = arra[i].errorCuadrado[5];
            data[i][14] = arra[i].variablesAleatoriasEstadistica[6];
            data[i][15] = arra[i].errorCuadrado[6];
            data[i][16] = arra[i].variablesAleatoriasEstadistica[7];
            data[i][17] = arra[i].errorCuadrado[7];
            data[i][18] = arra[i].variablesAleatoriasEstadistica[8];
            data[i][19] = arra[i].errorCuadrado[8];
        }
//        tabla.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        mdl = new DefaultTableModel(data, new Object[]{"Rango", "Frecuencia Observada", "Geometrica", "ECM", "Poisson", "ECM", "Binomial", "ECM", "Binomial negativa", "ECM", "Exponencial", "ECM", "Uniforme", "ECM", "Normal", "ECM", "Gamma", "ECM", "Logaritmica", "ECM"});

        tabla = new JTable(mdl);
        tabla.setPreferredScrollableViewportSize(new Dimension(450, 300));
        tabla.setFillsViewportHeight(true);
        tabla.setBounds(10, 0, 457, 103);
        panel = new JScrollPane(tabla);

    }

}
